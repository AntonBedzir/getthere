package test.java.my.progect.getthere.mock;

import main.java.my.progect.getthere.utils.Output;

public class OnMemoryOutput implements Output {

    private String printedData;

    public void print(Object s) {
        printedData = (String) s;
    }

    public String getPrintedData() {
        return printedData;
    }
}
