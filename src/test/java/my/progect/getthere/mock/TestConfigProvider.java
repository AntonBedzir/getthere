package test.java.my.progect.getthere.mock;

import main.java.my.progect.getthere.utils.Config;

public class TestConfigProvider implements Config {

    public String getDictionaryFileName() {
        return "src/test/resources/data/dictionary.txt";
    }
}
