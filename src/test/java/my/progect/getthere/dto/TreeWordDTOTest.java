package test.java.my.progect.getthere.dto;

import main.java.my.progect.getthere.dto.TreeWordDTO;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertThat;

public class TreeWordDTOTest {

    @Test
    public void testThatTreeWordDTOHasProperValuesAfterCreation() {
        String value = "test value";
        TreeWordDTO treeWordDTO = new TreeWordDTO(value);

        assertThat(treeWordDTO.getLevel(), equalTo(0));
        assertThat(treeWordDTO.getParentItem(), equalTo(null));
        assertThat(treeWordDTO.getChildrenItems().isEmpty(), equalTo(true));
        assertThat(treeWordDTO.getValue(), equalTo(value));
    }

    @Test
    public void testThatTreeWordDTOHasProperChildrenListAndParentItemAfterAddingItems() {
        String value = "test value";
        TreeWordDTO firstItem = new TreeWordDTO(value);

        TreeWordDTO secondItem = new TreeWordDTO(value);

        firstItem.addChildrenItem(secondItem);

        assertThat(firstItem.getChildrenItems(), hasItem(secondItem));
        assertThat(secondItem.getParentItem(), equalTo(firstItem));
        assertThat(secondItem.getLevel(), equalTo(1));
    }

}
