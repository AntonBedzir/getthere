package test.java.my.progect.getthere.utils;

import main.java.my.progect.getthere.utils.Config;
import main.java.my.progect.getthere.utils.DictionaryReader;
import org.junit.Test;
import test.java.my.progect.getthere.mock.TestConfigProvider;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class DictionaryReaderTest {

    private Config config = new TestConfigProvider();

    @Test
    public void testThatProperDataWillBeRead() {
        DictionaryReader dictionaryReader = new DictionaryReader(config);

        int wordSize = 3;
        List<String> actual = dictionaryReader.getDictionaryWords(wordSize);
        List<String> expected = Arrays.asList("foo", "bar");

        assertThat(actual, equalTo(expected));
    }
}
