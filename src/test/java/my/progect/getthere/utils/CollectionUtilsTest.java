package test.java.my.progect.getthere.utils;

import main.java.my.progect.getthere.utils.CollectionUtils;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class CollectionUtilsTest {

    @Test
    public void testThatListIsConvertedToArrayCorrect() {
        String[] array = {"a", "b", "s", "w"};
        List<String> list = Arrays.asList(array);
        assertThat(array, equalTo(CollectionUtils.getListAsArray(list)));
    }
}
