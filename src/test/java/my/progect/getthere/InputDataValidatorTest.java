package test.java.my.progect.getthere;

import main.java.my.progect.getthere.InputDataValidator;
import org.junit.BeforeClass;
import org.junit.Test;
import test.java.my.progect.getthere.mock.OnMemoryOutput;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class InputDataValidatorTest {

    private final static String NOT_EXPECTED_NUMBER_OF_ARGUMENTS = "Please provide start and end words";
    private final static String DIFFERENT_SIZE_ERROR_MESSAGE = "Start and end words should have the same length";

    private static OnMemoryOutput output;
    private static InputDataValidator inputDataValidator;

    @BeforeClass
    public static void initialize() {
        output = new OnMemoryOutput();
        inputDataValidator = new InputDataValidator(output);
    }

    @Test
    public void testThatEmptyInputHasCorrectResponse() {
        assertThat(inputDataValidator.areArgumentsValid(), equalTo(false));
        assertThat(output.getPrintedData(), equalTo(NOT_EXPECTED_NUMBER_OF_ARGUMENTS));
    }

    @Test
    public void testThatInputWithOneArgumentHasCorrectResponse() {
        assertThat(inputDataValidator.areArgumentsValid("Test"), equalTo(false));
        assertThat(output.getPrintedData(), equalTo(NOT_EXPECTED_NUMBER_OF_ARGUMENTS));
    }

    @Test
    public void testThatInputWithTwoWordsWithDiffSizeHasCorrectResponse() {
        assertThat(inputDataValidator.areArgumentsValid("Test", "Foo"), equalTo(false));
        assertThat(output.getPrintedData(), equalTo(DIFFERENT_SIZE_ERROR_MESSAGE));
    }

    @Test
    public void testThatInputWithCorrectData() {
        assertThat(inputDataValidator.areArgumentsValid("bar", "Foo"), equalTo(true));
        assertThat(output.getPrintedData(), equalTo(null));
    }
}
