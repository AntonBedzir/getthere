package test.java.my.progect.getthere;

import main.java.my.progect.getthere.GetThereResolver;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;


@RunWith(Parameterized.class)
public class GetThereResolverTest {

    private String start;
    private String finish;
    private List<String> dictionary;
    private List<String> result;

    @Parameters
    public static Object[][] provideStringAndExpectedLength() {
        return new Object[][]{
                {"cat", "car", "fet, dog, fff", "cat, car"},
                {"Cat", "Cor", "fet, dog, fff, car", "Cat, car, Cor"},
                {"cat", "dog", "rog, was, fog, fff, rot, rat", "cat, rat, rot, rog, dog"},
                {"cat", "dog", "cor, dor, car, dog, car, cor, dor, car", "cat, car, cor, dor, dog"},
                {"cat", "dog", "cat, fog", ""}
        };
    }

    public GetThereResolverTest(String start, String finish, String dictionary, String result) {
        this.start = start;
        this.finish = finish;
        this.dictionary = new ArrayList<>(Arrays.asList(dictionary.split(", ")));
        this.result = result.isEmpty() ? new ArrayList<>() : Arrays.asList(result.split(", "));
    }

    @Test
    public void testThatGetHereResolverReturnProperPath() {
        List<String> actualList = GetThereResolver.getMinPath(start, finish, dictionary);
        assertThat(actualList, equalTo(result));

    }

}
