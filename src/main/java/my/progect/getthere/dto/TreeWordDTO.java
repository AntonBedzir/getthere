package main.java.my.progect.getthere.dto;

import java.util.ArrayList;
import java.util.List;

public class TreeWordDTO {

    private int level;
    private String value;
    private TreeWordDTO parentItem;
    private List<TreeWordDTO> childrenItems;

    public TreeWordDTO(String value) {
        this.value = value;
        this.level = 0;
        childrenItems = new ArrayList<>();
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public TreeWordDTO getParentItem() {
        return parentItem;
    }

    public List<TreeWordDTO> getChildrenItems() {
        return childrenItems;
    }

    public void addChildrenItem(TreeWordDTO children) {
        children.parentItem = this;
        children.setLevel(this.level + 1);
        childrenItems.add(children);
    }

    public boolean hasParent() {
        return parentItem != null;
    }
}
