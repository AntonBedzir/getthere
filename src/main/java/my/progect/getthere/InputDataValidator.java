package main.java.my.progect.getthere;

import main.java.my.progect.getthere.utils.Output;

public class InputDataValidator {

    private Output output;

    public InputDataValidator(Output output) {
        this.output = output;
    }

    public boolean areArgumentsValid(String... args) {
        if (args.length < 2) {
            output.print("Please provide start and end words");
            return false;
        }
        if (args[0].length() != args[1].length()) {
            output.print("Start and end words should have the same length");
            return false;
        }
        return true;
    }
}
