package main.java.my.progect.getthere.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader implements Config {

    private final static String PATH_TO_CONFIG_FILE = "src/main/resources/config/config.properties";

    private static Properties properties = new Properties();

    static {
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(PATH_TO_CONFIG_FILE));
            properties.load(bufferedReader);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * getDictionaryFileName
     *
     * @return String
     */
    public String getDictionaryFileName() {
        return properties.getProperty("dictionary_file");
    }

}
