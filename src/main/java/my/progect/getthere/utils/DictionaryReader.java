package main.java.my.progect.getthere.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class DictionaryReader {

    private Config configReader;

    /**
     * DictionaryReader
     *
     * @param configReader -
     */
    public DictionaryReader(Config configReader) {
        this.configReader = configReader;
    }

    /**
     * getDictionary
     * Get list of words with specified length.
     *
     * @param wordLength -
     * @return getDictionary
     */

    public List<String> getDictionaryWords(int wordLength) {
        return getAllWordsFromDictionaryFile(configReader.getDictionaryFileName())
                .stream()
                .filter(stringHasLength(wordLength))
                .distinct()
                .collect(Collectors.toList());
    }

    private static List<String> getAllWordsFromDictionaryFile(String fileName) {
        List<String> words = new ArrayList<>();
        BufferedReader bufferedReader = null;

        try {
            bufferedReader = new BufferedReader(new FileReader(fileName));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                words.add(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return words;
    }

    private static Predicate<String> stringHasLength(int length) {
        return t -> t.length() == length;
    }

}
