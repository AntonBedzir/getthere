package main.java.my.progect.getthere.utils;

import java.util.List;

public class CollectionUtils {

    public static <T> T[] getListAsArray(List<T> list) {
        return list.toArray((T[]) new Object[list.size()]);
    }
}
