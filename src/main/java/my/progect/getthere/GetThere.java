package main.java.my.progect.getthere;

import main.java.my.progect.getthere.utils.ConfigReader;
import main.java.my.progect.getthere.utils.DictionaryReader;
import main.java.my.progect.getthere.utils.OnConsoleOutput;
import main.java.my.progect.getthere.utils.Output;

import java.util.List;

public class GetThere {

    public static void main(String[] args) {

        DictionaryReader dictionaryReader = new DictionaryReader(new ConfigReader());
        Output output = new OnConsoleOutput();
        InputDataValidator inputDataValidator = new InputDataValidator(output);

        if (inputDataValidator.areArgumentsValid(args)) {

            String firstWord = args[0];
            String secondWord = args[1];

            List<String> dictionary = dictionaryReader.getDictionaryWords(firstWord.length());


            List<String> result = GetThereResolver.getMinPath(firstWord, secondWord, dictionary);

            if (result.isEmpty()) {
                output.print("It is not possible to transform " + firstWord + " into " + secondWord);
            } else {
                output.print(result);
            }
        }
    }


}
