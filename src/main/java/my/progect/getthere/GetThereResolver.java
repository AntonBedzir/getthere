package main.java.my.progect.getthere;

import main.java.my.progect.getthere.dto.TreeWordDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class GetThereResolver {

    public static List<String> getMinPath(String start, String finish, List<String> dictionary) {

        dictionary.add(finish);

        TreeWordDTO lastItem;
        TreeWordDTO firstItem = new TreeWordDTO(start);
        List<TreeWordDTO> currentLevelItems = new ArrayList<>();
        currentLevelItems.add(firstItem);

        do {
            for (TreeWordDTO item : currentLevelItems) {
                addChildrenItems(item, dictionary);
            }
            currentLevelItems = getNextLevelItems(currentLevelItems);
            lastItem = getItemFromListWithExpectedValue(finish, currentLevelItems);
        }
        while (lastItem == null && !currentLevelItems.isEmpty() && currentLevelItems.get(0).getLevel() < dictionary.size());

        return lastItem == null ? new ArrayList<>() : getPathFromStartToFinish(lastItem);
    }

    private static boolean isCorrectStep(String str1, String str2) {
        int counter = 0;
        for (int index = 0; index < str1.length(); index++) {
            if (str1.toLowerCase().charAt(index) != str2.toLowerCase().charAt(index)) {
                counter++;
            }
        }
        return counter == 1;
    }

    private static void addChildrenItems(TreeWordDTO item, List<String> dictionary) {
        for (String word : dictionary) {
            if (isCorrectStep(item.getValue(), word) && (item.getParentItem() == null
                    || !item.getParentItem().getValue().equals(word))) {
                item.addChildrenItem(new TreeWordDTO(word));
            }
        }
    }

    private static List<TreeWordDTO> getNextLevelItems(List<TreeWordDTO> currentLevelItems) {
        List<TreeWordDTO> nextLevelItems = new ArrayList<>();
        for (TreeWordDTO item : currentLevelItems) {
            nextLevelItems.addAll(item.getChildrenItems());
        }
        return nextLevelItems;
    }

    private static TreeWordDTO getItemFromListWithExpectedValue(String finish, List<TreeWordDTO> items) {
        return items.stream()
                .filter(item -> item.getValue().equals(finish))
                .findFirst()
                .orElse(null);
    }

    private static List<String> getPathFromStartToFinish(TreeWordDTO finishItem) {
        List<String> values = new LinkedList<>();
        getValue(finishItem, values);
        Collections.reverse(values);
        return values;
    }

    private static List<String> getValue(TreeWordDTO item, List<String> values) {
        values.add(item.getValue());
        if (item.hasParent()) {
            return getValue(item.getParentItem(), values);
        } else {
            return values;
        }
    }


}
